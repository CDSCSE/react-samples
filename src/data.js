export const user = {
    name: 'Addison William',
    alerts: 2,
};

export const companyInfo = {
    'Company Name': 'Quisquam',
    'Customer ID': 'Quis025182',
    'AX ID': '3275489078',
    'MSR ID': '190832423928',
};

export const accountTeamInfo = {
    'Account Manager': 'John Smith',
    'Service Delivery Manager': 'Jane Doe',
    'Managed Service Engineer': 'Steve Wozniak',
    'Networks Engineer': 'Bill Gates',
};

export const sidebar = {
    overview: [
        {
            text: 'Admin Dashboard',
            icon: 'home',
        },
    ],
    services: [
        {
            text: 'My Cloud',
            icon: 'cloud',
        },
        {
            text: 'Operations',
            icon: 'tablet',
        },
        {
            text: 'System',
            icon: 'desktop_mac',
        },
        {
            text: 'IaaS Report',
            icon: 'assignment',
            items: [
                'Inventory Report - Project 1',
                'Inventory Report - Project 2',
                'Inventory Report - Project 3',
                'Inventory Report - Project 4',
            ]
        },
    ]
};

export const inventoryData = [
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'DOLCGXIP-02',
        'Status': 'On',
        'CPU': 6,
        'RAM': 4,
        'DISK': 8,
        'Application': 'CONNECTWISE',
        'Environment': 'PROD',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'DOLCGXIP-03',
        'Status': 'Off',
        'CPU': 3,
        'RAM': 16,
        'DISK': 52,
        'Application': 'ADFS',
        'Environment': 'PROD',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'ESS-MIM01.local',
        'Status': 'Off',
        'CPU': 2,
        'RAM': 73,
        'DISK': 83,
        'Application': 'CALLCATCH',
        'Environment': 'DEV',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'DOLCGXIP-08',
        'Status': 'On',
        'CPU': 2,
        'RAM': 4,
        'DISK': 236,
        'Application': 'NABLE',
        'Environment': '-',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'ESS-SAN-01',
        'Status': 'Off',
        'CPU': 6,
        'RAM': 4,
        'DISK': 8,
        'Application': 'ECS',
        'Environment': 'DEV',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'MDT2013',
        'Status': 'On',
        'CPU': 6,
        'RAM': 67,
        'DISK': 682,
        'Application': '-',
        'Environment': 'DEV',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'DOLCGXIP-VPN',
        'Status': 'Off',
        'CPU': 4,
        'RAM': 16,
        'DISK': 231,
        'Application': 'SQL',
        'Environment': 'PROD',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'CTXDDC-01',
        'Status': 'On',
        'CPU': 6,
        'RAM': 4,
        'DISK': 8,
        'Application': 'BILLING',
        'Environment': 'PROD',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
    {
        'VM Name': 'DOLSDGLYEVCW02',
        'VM HostName': 'DEMO-DC1.demo',
        'Status': 'Off',
        'CPU': 7,
        'RAM': 63,
        'DISK': 10,
        'Application': 'EXCHANGES',
        'Environment': 'PROD',
        'CostCode': '-',
        'As Of': 'Mon, 03-Apr-2017 12:12:05',
    },
];
