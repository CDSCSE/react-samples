import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {makeStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import {
    Menu as MenuIcon,
    AddAlert as AlertIcon,
    ArrowDropDown as ExpandMoreIcon,
    ArrowDropUp as ExpandLessIcon,
} from '@material-ui/icons';
import logo from '../images/Logo.png';

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        background: 'white',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        '& span': {
            color: '#14A1C8'
        }
    },
    logo: {
        maxHeight: 40,
        marginRight: theme.spacing(1),
    },
}));

/**
 * Menu for account settings
 * Displays user's name and has dropdown for settings
 * @param {Object} props 
 * @returns {React.Component}
 */
function AccountMenu(props) {
    const [open, setOpen] = React.useState(false);

    function handleClick() {
        setOpen(!open);
    }

    return (
        <List>
            <ListItem button onClick={handleClick}>
                {props.name}
                {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </ListItem>
        </List>
    );
}

/**
 * Top bar that contains menu, title, alerts, and account menu
 * @param {Object} props
 * @return {React.Component}
 */
export default function TopBar(props) {
    const classes = useStyles();

    return (
        <AppBar position='fixed' color='default' className={classes.appBar}>
            <Toolbar>
                <IconButton edge='start' className={classes.menuButton} color='inherit' aria-label='Menu'>
                    <MenuIcon />
                </IconButton>
                <img src={logo} alt={'logo'} className={classes.logo} />
                <Typography variant='h6' className={classes.title}>
                    <span>Cloud</span> Platform
                </Typography>
                <IconButton className={classes.menuButton} color='inherit' aria-label='Alerts'>
                    <Badge badgeContent={props.alerts} color='secondary'>
                        <AlertIcon />
                    </Badge>
                </IconButton>
                <AccountMenu {...props} />
            </Toolbar>
        </AppBar>
    );
}
