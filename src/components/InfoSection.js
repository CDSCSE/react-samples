import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    divider: {
        height: theme.spacing(0.25),
        background: '#979797',
    },
    label: {
        flexGrow: 1,
    },
    data: {
        color: '#6091CA',
    },
    infoSectionItem: {
        padding: theme.spacing(0.5),
    },
}));

/**
 * Info section that displays data beneath a title
 * @param {Object} props
 * @returns {React.Component}
 */
export default function InfoSection(props) {
    const classes = useStyles();

    return (
        <div>
            <Typography variant='body1'>
                {props.title}
            </Typography>
            <Divider className={classes.divider} />
            <List disablePadding>
                {Object.entries(props.info).map((entry, index) => (
                    <ListItem className={classes.infoSectionItem} key={index}>
                        <Typography variant='body2' className={classes.label}>
                            {entry[0]}:
                        </Typography>
                        <Typography variant='body2' className={classes.data}>
                            {entry[1]}
                        </Typography>
                    </ListItem>
                ))}
            </List>
        </div>
    );
}
