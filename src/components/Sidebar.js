import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import {
    DesktopMac as DesktopMacIcon,
    Cloud as CloudIcon,
    Tablet as TabletIcon,
    Home as HomeIcon,
    Assignment as AssignmentIcon,
    ChevronRight as ExpandMoreIcon,
    ExpandMore as ExpandLessIcon,
} from '@material-ui/icons';

//Set width of sidebar
const drawerWidth = 300;

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    paper: {
        width: drawerWidth,
        background: '#314056',
        color: 'white',
        paddingLeft: theme.spacing(1),
    },
    toolbar: theme.mixins.toolbar,
    icon: {
        color: '#6682A8',
    },
    listSubheader: {
        color: '#6682A8',
        fontSize: '1.25em',
    },
    nestedItem: {
        paddingLeft: theme.spacing(9),
        color: '#6682A8'
    },
}));

/**
 * Take in an icon's name and return the corresponding icon
 * @param {string} icon 
 * @returns {React.Component}
 */
const getIconByName = (icon) => {
    switch (icon) {
        case 'desktop_mac':
            return (<DesktopMacIcon />);
        case 'cloud':
            return (<CloudIcon />);
        case 'tablet':
            return (<TabletIcon />);
        case 'home':
            return (<HomeIcon />);
        case 'assignment':
            return (<AssignmentIcon />);
        default:
            return null;
    }
}

/**
 * Sidebar list item that expands to display subitems
 * @param {Object} props
 * @returns {React.Component}
 */
function ExpandableSidebarItem(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function handleClick() {
        setOpen(!open);
    }

    return (
        <List>
            <ListItem button onClick={handleClick}>
                <ListItemIcon className={classes.icon}>{getIconByName(props.icon)}</ListItemIcon>
                <ListItemText primary={props.text} />
                {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </ListItem>
            <Collapse in={open} timeout='auto' unmountOnExit>
                <List component='div' disablePadding>
                    {props.items && props.items.map((item, index) => (
                        <Link component='button' underline='none' key={index}>
                            <ListItem className={classes.nestedItem}>
                                <ListItemText primary={item} />
                            </ListItem>
                        </Link>
                    ))}
                </List>
            </Collapse>
        </List>
    );
}

/**
 * Sidebar component that contains 'Overview' and 'Service' sections
 * @param {Object} props
 * @returns {React.Component}
 */
export default function Sidebar(props) {
    const classes = useStyles();

    return (
        <Drawer
            variant='permanent'
            anchor='left'
            className={classes.drawer}
            classes={{
                paper: classes.paper,
            }}
        >
            <div className={classes.toolbar} />
            <List>
                <ListSubheader className={classes.listSubheader}>Overview</ListSubheader>
                {props.overview.map((item, index) => (
                    <ListItem button key={index}>
                        <ListItemIcon className={classes.icon}>{getIconByName(item.icon)}</ListItemIcon>
                        <ListItemText primary={item.text} />
                    </ListItem>
                ))}
                <ListSubheader className={classes.listSubheader}>Service</ListSubheader>
                {props.services.map((item, index) => (
                    <ExpandableSidebarItem {...item} key={index} />
                ))}
            </List>
        </Drawer>
    );
}
