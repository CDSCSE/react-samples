import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import StatusIcon from '@material-ui/icons/Brightness1';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import _ from 'underscore';
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    statusIconOn: {
        color: '#7DD420',
        marginRight: theme.spacing(1),
    },
    statusIconOff: {
        color: '#DE7372',
        marginRight: theme.spacing(1),
    },
    link: {
        color: '#4B8596',
    },
    totalsRow: {
        background: '#E1E5EA',
    },
    totalsCell: {
        fontWeight: 'bold',
    },
    shadedBackground: {
        background: '#F1F5F9',
    },
    table: {
        background: 'white',
    },
    tableRow: {
        '&:nth-child(even)': {
            background: '#F1F5F9',
        },
        border: '1px solid #E1E5EA',
    }
}));

/**
 * Displays a table beneath a title
 * @param {Object} props
 * @returns {React.Component}
 */
export default function TableSection(props) {
    const classes = useStyles();

    /**
     * Returns a table cell formatted based on the data it displays
     * @param {*} data 
     * @param {number} index 
     * @returns {React.Component}
     */
    const _getCell = (data, index) => {
        return (
            <TableCell key={index}>
                {index === 2 && <StatusIcon fontSize='inherit' className={data === 'On' ? classes.statusIconOn : classes.statusIconOff} />}
                {index === 0 ? (<Link component='button' underline='none' className={classes.link}>{data}</Link>) : data}
            </TableCell>
        );
    };

    //Calculations for Totals row
    const onlineVMs = _.where(props.data, {Status: 'On'}).length;
    const totalVMs = props.data.length;
    const totalCPU = _.reduce(props.data, (total, data) => total + data.CPU, 0);
    const totalRAM = _.reduce(props.data, (total, data) => total + data.RAM, 0);
    const totalDISK = _.reduce(props.data, (total, data) => total + data.DISK, 0);

    return (
        <div className={classes.table}>
            <Typography variant='body1' className={classes.shadedBackground}>
                {props.title}
            </Typography>
            <Table>
                <TableHead className={classes.shadedBackground}>
                    <TableRow>
                        {props.headers.map((header, index) => (
                            <TableCell key={index}>{header}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.data.map((row, index) => (
                        <TableRow key={index} className={classes.tableRow}>
                            {props.headers.map((header, index) => _getCell(row[header], index))}
                        </TableRow>
                    ))}
                    <TableRow className={classes.totalsRow}>
                        <TableCell className={classes.totalsCell}>Total</TableCell>
                        <TableCell className={classes.totalsCell}>{`${onlineVMs}/${totalVMs} VM's Online`}</TableCell>
                        <TableCell></TableCell>
                        <TableCell className={classes.totalsCell}>{totalCPU}</TableCell>
                        <TableCell className={classes.totalsCell}>{totalRAM}</TableCell>
                        <TableCell className={classes.totalsCell}>{totalDISK}</TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </div>
    );
}
