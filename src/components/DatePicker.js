import 'date-fns';
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import EventIcon from '@material-ui/icons/Event';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    datePickerIcon: {
        color: '#12A2CA',
    },
}));

/**
 * Create date picker component
 * @returns {React.Component}
 */
export default function DatePicker() {
    const classes = useStyles();
    const [selectedDate, handleDateChange] = React.useState(null);

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
                margin='normal'
                id='mui-pickers-date'
                emptyLabel='Select Date'
                value={selectedDate}
                onChange={date => handleDateChange(date)}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                keyboardIcon={<EventIcon className={classes.datePickerIcon} />}
            />
        </MuiPickersUtilsProvider>
    );
}
