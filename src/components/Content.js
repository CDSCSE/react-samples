import 'date-fns';
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import DatePicker from './DatePicker';
import InfoSection from './InfoSection';
import TableSection from './TableSection';

//Create styles that will be used in this file
const useStyles = makeStyles(theme => ({
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        padding: theme.spacing(4),
        background: 'white',
    },
    appBarSpacer: theme.mixins.toolbar,
    grid: {
        paddingTop: theme.spacing(2),
    },
    datePickerContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    shadedBackground: {
        background: '#F1F5F9',
    },
    title: {
        color: '#666666',
    },
}));

/**
 * Content section that contains title, date picker, company info, account team info, and IaaS Inventory Data
 * @param {Object} props
 * @returns {React.Component}
 */
export default function Content(props) {
    const classes = useStyles();

    return (
        <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Typography variant='h5' className={classes.title}>
                Iaas Inventory Report
            </Typography>
            <Box className={classes.datePickerContainer}>
                <DatePicker />
            </Box>
            <Grid container spacing={6} className={classes.grid}>
                <Grid item xs={6}>
                    <InfoSection title='Company Information' info={props.companyInfo} />
                </Grid>
                <Grid item xs={6}>
                    <InfoSection title='Account Team' info={props.accountTeamInfo} />
                </Grid>
                <Grid item xs={12} className={classes.shadedBackground}>
                    <TableSection
                        title='IaaS Inventory Data'
                        data={props.inventoryData}
                        headers={['VM Name', 'VM HostName', 'Status', 'CPU', 'RAM', 'DISK', 'Application', 'Environment', 'CostCode', 'As Of']}
                    />
                </Grid>
                <Grid item xs={12} className={classes.shadedBackground}>
                    <TableSection
                        title='IaaS Inventory Data'
                        data={props.inventoryData}
                        headers={['VM Name', 'VM HostName', 'Status', 'CPU', 'RAM', 'DISK', 'Application', 'Environment', 'CostCode', 'As Of']}
                    />
                </Grid>
            </Grid>
        </main>
    );
}
