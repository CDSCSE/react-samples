import 'typeface-roboto';
import React from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import {makeStyles} from '@material-ui/core/styles';
import TopBar from './components/TopBar';
import Sidebar from './components/Sidebar';
import Content from './components/Content';

//Temporarily get fake data from file
import {
  user,
  sidebar,
  companyInfo,
  accountTeamInfo,
  inventoryData,
} from './data';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
}));

/**
 * SPA that consists of header, sidebar, and content section
 */
function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <TopBar {...user} />
      <Sidebar {...sidebar} />
      <Content companyInfo={companyInfo} accountTeamInfo={accountTeamInfo} inventoryData={inventoryData} />
    </div>
  );
}

export default App;
